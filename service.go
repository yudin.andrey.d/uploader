package uploader

import (
	"crypto/md5"
	"database/sql"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var Incomplete = errors.New("Incomplete ")

type Uploader struct {
	log    log.Logger
	db     sql.Conn
	Root   string
	Meta   *MetaData
	Body   *Body
	storer Storer
}

// Function SaveFiles sequentially loads the original files or chunk's.
func (up *Uploader) SaveFiles() ([]*File, error) {
	files := make([]*File, 0)

	for {
		ofile, err := up.SaveFile()
		if err == io.EOF {
			break
		}

		if err == Incomplete {
			files = append(files, ofile)
			return files, err
		}

		if err != nil {
			return nil, err
		}

		files = append(files, ofile)
	}

	return files, nil
}

func (up *Uploader) SaveFile() (*File, error) {
	body, filename, err := up.Reader()
	if err != nil {
		return nil, err
	}

	temp_file, err := up.TempFile()
	if err != nil {
		return nil, err
	}
	defer temp_file.Close()

	if err = up.Write(temp_file, body); err != nil {
		return nil, err
	}

	fi, err := temp_file.Stat()
	if err != nil {
		return nil, err
	}

	ofile := &File{Filename: filename, Filepath: temp_file.Name(), Size: fi.Size()}

	if up.Meta.Range != nil && ofile.Size != up.Meta.Range.Size {
		return ofile, Incomplete
	}

	ofile.BaseMime, err = IdentifyMime(ofile.Filepath)
	if err != nil {
		return nil, err
	}

	return ofile, nil
}

// The function writes a temporary file value from reader.
func (up *Uploader) Write(temp_file *os.File, body io.Reader) error {
	var err error
	if up.Meta.Range == nil {
		_, err = io.Copy(temp_file, body)
	} else {
		chunk_size := up.Meta.Range.End - up.Meta.Range.Start + 1
		_, err = io.CopyN(temp_file, body, chunk_size)
	}
	return err
}

// Returns a temporary file to download the file or resume chunk.
func (up *Uploader) TempFile() (*os.File, error) {
	if up.Meta.Range == nil {
		return TempFile()
	}
	return TempFileChunks(up.Meta.Range.Start, up.Root, up.Meta.Filename)
}

// Returns the reader to read the file or chunk of request body and the original file name.
// If the request header Conent-Type is multipart/form-data, returns the next copy part.
// If all of part read the case of binary loading read the request body, an error is returned io.EOF.
func (up *Uploader) Reader() (io.Reader, string, error) {
	if up.Meta.MediaType == "multipart/form-data" {
		if up.Body.MR == nil {
			up.Body.MR = multipart.NewReader(up.Body.Body, up.Meta.Boundary)
		}
		for {
			part, err := up.Body.MR.NextPart()
			if err != nil {
				return nil, "", err
			}
			if part.FormName() == "files[]" {
				return part, part.FileName(), nil
			}
		}
	}

	if up.Body.Available == false {
		return nil, "", io.EOF
	}

	up.Body.Available = false

	return up.Body.Body, up.Meta.Filename, nil
}

type File struct {
	Size     int64
	BaseMime string
	Filename string
	Filepath string
}

func (f *File) Ext() string {
	return strings.ToLower(filepath.Ext(f.Filename))
}

// Get base mime type
//		$ file --mime-type pic.jpg
//		pic.jpg: image/jpeg
func IdentifyMime(file string) (string, error) {
	out, err := exec.Command("file", "--mime-type", file).CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("identify: err: %s; detail: %s", err, string(out))
	}

	mime := strings.Split(strings.Split(string(out), ": ")[1], "/")[0]

	return mime, nil
}

// Returns the newly created temporary file.
func TempFile() (*os.File, error) {
	return ioutil.TempFile(os.TempDir(), "pavo")
}

// Returns a temporary file to download chunk.
// To calculate a unique file name used cookie named pavo and the original file name.
// File located in the directory chunks storage root directory.
// Before returning the file pointer is shifted by the value of offset,
// in a situation where the pieces are loaded from the second to the last.
func TempFileChunks(offset int64, storage, user_filename string) (*os.File, error) {
	hasher := md5.New()
	hasher.Write([]byte(user_filename))
	filename := hex.EncodeToString(hasher.Sum(nil))

	path := filepath.Join(storage, "chunks")

	err := os.MkdirAll(path, 0755)
	if err != nil {
		return nil, err
	}

	file, err := os.OpenFile(filepath.Join(path, filename), os.O_CREATE|os.O_WRONLY, 0664)
	if err != nil {
		return nil, err
	}

	if _, err = file.Seek(offset, 0); err != nil {
		return nil, err
	}

	return file, nil
}

package main

import (
	"fmt"
	"log"
	"net/http"
	"uploader/events"
	"uploader/handlers"
)

const serverPortEnv = "8080"
const UploaderPath = "/upload"

func main() {
	errChan := make(chan error)
	go startHealthServices()

	pub := events.NewRabbitPublisher()
	upload := handlers.NewUploaderHandler(pub)

	startHTTPService := func(errCh chan error) {
		http.HandleFunc(UploaderPath, upload.Process)

		serverPort := fmt.Sprintf(":%s", serverPortEnv)

		log.Print("uploader start ", "port:", serverPortEnv)
		errChan <- http.ListenAndServe(serverPort, nil)

	}
	go startHTTPService(errChan)

	select {
	case err := <-errChan:
		log.Fatal(err)
	}
}

func startHealthServices() {
	err := handlers.ServeHealth()
	if err != nil {
		panic(err.Error())
	}
}

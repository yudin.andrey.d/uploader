package uploader

import (
	"errors"
	"mime"
	"net/http"
)

// MetaData -
type MetaData struct {
	Range     *Range
	Filename  string
	Boundary  string
	MediaType string
}

func ParseMetaData(req *http.Request) (parsedMD *MetaData, err error) {
	md := &MetaData{}

	if err := md.parseContentType(req.Header.Get("Content-Type")); err != nil {
		return md, err
	}
	if err := md.parseRange(req.Header.Get("Content-Range")); err != nil {
		return md, err
	}

	return md, err
}

func (md *MetaData) parseContentType(contentType string) error {
	mediaType, params, err := mime.ParseMediaType(contentType)

	if err != nil {
		return err
	}

	if mediaType == "multipart/form-data" {
		boundary, ok := params["boundary"]
		if !ok {
			return errors.New("boundary nor define")
		}
		md.MediaType = mediaType
		md.Boundary = boundary
	} else {
		md.MediaType = "application/octet-stream"
	}
	return nil
}

func (md *MetaData) parseRange(contentRange string) error {
	if contentRange == "" {
		return nil
	}
	return nil
}

// Range - internal struct for store big files part-by-part
type Range struct {
	Start int64
	End   int64
	Size  int64
}

//func (m *MetaData) parseContentRange(get string) error {
//	if get == "" {
//		return nil
//	}
//	var start, end, size int64
//
//	_, err := fmt.Sscanf(get, "bytes %d-%d/%d", &start, &end, &size)
//	if err != nil {
//		return err
//	}
//	m.Range = &Range{Start: start, End: end, Size: size}
//	return nil
//}

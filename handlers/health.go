package handlers

import (
	"net/http"
	"uploader/env"
)

func ServeHealth() error {
	port := env.Get("HEALTHCHECK_PORT").String(":80")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {})

	return http.ListenAndServe(port, nil)
}

package handlers

import (
	"fmt"
	"log"
	"net/http"
	"uploader"
	"uploader/events"
)

type UploaderHandler struct {
	//log log.Logger
	pub events.Publisher
}

func NewUploaderHandler(pub events.Publisher) *UploaderHandler {
	return &UploaderHandler{pub: pub}
}

func (u UploaderHandler) Process(w http.ResponseWriter, r *http.Request) {
	meta, err := uploader.ParseMetaData(r)
	fmt.Println(meta, err)

	body, err := uploader.NewBody(r.Header.Get("X-File"), r.Body)
	if err != nil {
		log.Fatal(err)
	}
	up := &uploader.Uploader{Root: "/", Meta: meta, Body: body}
	file, err := up.SaveFiles()
	if err == uploader.Incomplete {
		w.WriteHeader(500)
	}
	if err != nil {
		w.WriteHeader(500)
	}
	w.Write([]byte(fmt.Sprint("upload: ", len(file))))

	//Publish event after upload file
	err = u.pub.SendFile()
	if err != nil {
		log.Println("error")
	}
}

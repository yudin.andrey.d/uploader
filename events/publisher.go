package events

import "log"

type Publisher interface {
	SendFile() error
}

type publisher struct {
	//log log.Logger
	//mq  mq.Client
}

func NewRabbitPublisher() *publisher {
	return &publisher{}
}

func (receiver *publisher) SendFile() error {
	log.Println("Publish file upload event to rabbit mq")
	return nil
}
